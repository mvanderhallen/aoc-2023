{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as P
import qualified Text.Parsec.Char as C
import Text.Parsec.Language (haskellDef)

import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
import qualified Puzzle as Puzzle

type PuzzleType = [[Integer]]

inputParser :: Parser PuzzleType
inputParser = do
  many (symbol "|" *> many (signed natural))

process :: [Integer] -> [Integer]
process input 
  | all (==0) input = []
  | otherwise = last input : (process $ differences input)

differences :: Num a => [a] -> [a]
differences xs = zipWith (subtract) xs (drop 1 xs)

assignment1 :: PuzzleType -> Integer
assignment1 = sum . map (sum . process)

process' :: [Integer] -> [[Integer]]
process' input
  | all (==0) input = []
  | otherwise = input : process' (differences input)

assignment2 :: PuzzleType -> Integer
assignment2 = sum . map computePrecedent
  where
    computePrecedent = foldr (\(x:_) acc -> x-acc) 0 . process'

instance Puzzle.Puzzle PuzzleType where
  parseInput = inputParser
  assignment1 = show . assignment1
  assignment2 = show . assignment2

main :: IO ()
main = Puzzle.doMain @PuzzleType

interactive :: FilePath -> IO ()
interactive = Puzzle.interactive @PuzzleType
