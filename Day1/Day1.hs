module Day1 where

import Data.Char (isDigit)
import Data.List (tails)
import Data.Maybe (fromJust)
import Data.Monoid (First (First, getFirst), Last (Last, getLast))

assignment1 :: IO ()
assignment1 = do
  integers <- map parse <$> input :: IO [Int]
  print $ sum integers
  return ()

parse :: [Char] -> Int
parse = combine . foldl accumulate (First Nothing, Last Nothing)
  where 
    accumulate acc c = if isDigit c 
      then acc <> (First $ Just $ read [c], Last $ Just $ read [c]) 
      else acc

assignment2 :: IO ()
assignment2 = do
  integers <- map parse2 <$> input :: IO [Int]
  print $ sum integers
  return ()

parse2 :: String -> Int
parse2 = combine . foldl accumulate (First Nothing, Last Nothing) . tails
  where
    accumulate acc c = acc <> (First $ toDigit c, Last $ toDigit c)

combine :: Num a => (First a, Last a) -> a
combine (f,l) = fromJust (getFirst f) *10 +  fromJust (getLast l)

toDigit :: [Char] -> Maybe Int
toDigit ('o' : 'n' : 'e' : _)             = Just 1
toDigit ('t' : 'w' : 'o' : _)             = Just 2
toDigit ('t' : 'h' : 'r' : 'e' : 'e' : _) = Just 3
toDigit ('f' : 'o' : 'u' : 'r' : _)       = Just 4
toDigit ('f' : 'i' : 'v' : 'e' : _)       = Just 5
toDigit ('s' : 'i' : 'x' : _)             = Just 6
toDigit ('s' : 'e' : 'v' : 'e' : 'n' : _) = Just 7
toDigit ('e' : 'i' : 'g' : 'h' : 't' : _) = Just 8
toDigit ('n' : 'i' : 'n' : 'e' : _)       = Just 9
toDigit (x : _)                           = if isDigit x then Just $ read [x] else Nothing
toDigit []                                = Nothing

input :: IO [String]
input = lines <$> readFile "input.txt"

main :: IO ()
main = do
  assignment1
  assignment2
