{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as P
import qualified Text.Parsec.Char as C
import Text.Parsec.Language (haskellDef)

import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
import qualified Puzzle as Puzzle

import Data.List (intersect)
import Data.Maybe (fromJust)
import qualified Data.IntMap as M

type PuzzleType = [Card]
data Card = MkCard Int [Integer] [Integer] deriving (Show, Eq)

inputParser :: Parser PuzzleType
inputParser = many parseCard

parseCard = do
  symbol "Card"
  cardId <- fromIntegral <$> natural
  symbol ":"
  winning <- many natural
  symbol "|"
  card <- many natural
  return $ MkCard cardId winning card

assignment1 :: PuzzleType -> Int
assignment1 = sum . map count
  where
    count (MkCard _ winning nbs) = case nbs `intersect` winning of
      [] -> 0
      xs -> 2 ^ (length xs - 1)

assignment2 :: PuzzleType -> Int
assignment2 cards = sum $ map snd $ M.toList $ simulate (M.fromAscList [(cardId, 1)|cardId<-[1..length cards]]) cards
  where
    simulate acc ((MkCard cardId w a):cards) = case w `intersect` a of
      [] -> simulate acc cards
      xs -> do
        let multiplier = fromJust $ M.lookup cardId acc
        let acc' = foldl (\acc k -> M.adjust (+multiplier) k acc) acc [cardId+1 .. cardId+length xs]
        simulate acc' cards
    simulate acc [] = acc

instance Puzzle.Puzzle PuzzleType where
  parseInput = inputParser
  assignment1 = show . assignment1
  assignment2 = show . assignment2

main :: IO ()
main = Puzzle.doMain @PuzzleType

interactive :: FilePath -> IO ()
interactive = Puzzle.interactive @PuzzleType
