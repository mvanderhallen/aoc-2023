{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as P
import qualified Text.Parsec.Char as C
import Text.Parsec.Language (haskellDef)

-- imports lexeme, symbol, natural, int, signed, decimal, whiteSpace, Parser a,
-- getAOCInput, interactive, criterion and doMain
import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
import qualified Puzzle as Puzzle
import Data.List (sort)

type PuzzleType = ([Integer],[[Mapping]])

data Mapping = Mapping Integer Integer Integer deriving (Show, Eq)

parseMapping :: Parser Mapping
parseMapping = Mapping <$> natural <*> natural <*> natural

parseMaps :: Parser [[Mapping]]
parseMaps = many $ do
  many (oneOf "abcdefghijklmnopqrstuvwxyz- ")
  symbol ":"
  many1 parseMapping

inputParser :: Parser PuzzleType
inputParser = do
  symbol "seeds:"
  (,) <$> many1 natural <*> parseMaps

assignment1 :: PuzzleType -> Integer
assignment1 (seeds, maps) = minimum $ foldl lookups seeds maps
  where
    lookups values mapping = map (find mapping) values

between :: Integer -> Integer -> Integer -> Bool
between start length value = value >= start && value < (start+length)

find :: [Mapping] -> Integer -> Integer
find ((Mapping x y z):ms) v
  | Main.between y z v = x + (v-y)
  | otherwise          = find ms v
find [] v = v

assignment2 :: PuzzleType -> Integer
assignment2 (seedRanges, maps) = minimum $ go seedRanges
  where
    go :: [Integer] -> [Integer]
    -- Take two numbers from the list, and run it through the mappings with a modified 'find' that is 'range' aware: 
    -- it returns the start of a resulting range, and its length. If that length is smaller than the length of the input range, we
    -- recurse with a synthetic input prepended.
    go (seed:len:rem) = do
      let (res, len') = foldl lookups (seed,len) maps
      if len' < len then
        res : go (seed+len':len-len':rem)
      else
        res : go rem
    go [] = []
    lookups values mapping = find' mapping mapping values


find' :: [Mapping] -> [Mapping] -> (Integer, Integer) -> (Integer, Integer)
find' all ((Mapping x y z):ms) (v,v')
  | Main.between y z v = (x + (v-y), min (y+z-v) v')
  | otherwise          = find' all ms (v,v')
find' all [] (v,v') = do
  let x = sort $ [y| Mapping _ y _ <-all, y > v]
  case x of
    (h:_) -> (v, min (v+v') (h-v))
    []    -> (v,v')

instance Puzzle.Puzzle PuzzleType where
  parseInput = inputParser
  assignment1 = show . assignment1
  assignment2 = show . assignment2

main :: IO ()
main = Puzzle.doMain @PuzzleType

interactive :: FilePath -> IO ()
interactive = Puzzle.interactive @PuzzleType
