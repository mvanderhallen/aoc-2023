{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

import Text.Parsec hiding (State)
import qualified Text.Parsec.Token as P
import qualified Text.Parsec.Char as C
import Text.Parsec.Language (haskellDef)
import Data.List (elemIndex, group, sort)
import Data.Maybe (fromJust)

import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
import qualified Puzzle as Puzzle

data HandType = High | Pair | TwoPair | Three | FullHouse | Four | Five deriving (Show, Eq, Ord)
data Hand = MkHand HandType String deriving (Show, Eq)

compareChar :: Char -> Char -> Ordering
compareChar lhs rhs = compare (fromJust $ elemIndex lhs order) (fromJust $ elemIndex rhs order)
  where
    order = reverse "AKQJT98765432"

instance Ord Hand where
  compare (MkHand t1 lhs) (MkHand t2 rhs) = compare t1 t2 `mappend` (foldl1 mappend $ zipWith compareChar lhs rhs)

type PuzzleType = [(Hand, Integer)]

inputParser :: Parser PuzzleType
inputParser =
  many ((,) <$> parseHand <*> natural)

parseHand :: Parser Hand
parseHand = do
  hand <- lexeme $ count 5 (oneOf "AKQJT98765432")
  return $ MkHand (determineType hand) hand

determineType :: String -> HandType
determineType s = case (reverse $ sort $ map length $ group $ sort s) of
  [5] -> Five
  (4:_) -> Four
  [3,2] -> FullHouse
  (3:_) -> Three
  (2:2:_) -> TwoPair
  (2:_) -> Pair
  _ -> High

result :: forall a. (a, Integer) -> Integer -> Integer
result (_,bid) n = n * bid

assignment1 :: PuzzleType -> Integer
assignment1 input = sum $ zipWith result (sort input) [1..]

assignment2 :: PuzzleType -> Integer
assignment2 input = sum $ zipWith result (sort redetermined) [1..]
  where
    redetermined = map (\((MkHand _ s),b) -> (MkHand' (redetermine s) s,b)) input

data Hand' = MkHand' HandType String deriving (Show, Eq)

instance Ord Hand' where
  compare (MkHand' t1 lhs) (MkHand' t2 rhs) = compare t1 t2 `mappend` (foldl1 mappend $ zipWith compareChar lhs rhs)
    where
      compareChar lhs rhs =compare (fromJust $ elemIndex lhs order) (fromJust $ elemIndex rhs order)
      order = reverse "AKQT98765432J"

redetermine :: String -> HandType
redetermine s = case (determineType $ filter (/= 'J') s, length $ filter (=='J') s) of
  (_,    5)   -> Five
  (_,    4)   -> Five
  (Pair, 3)   -> Five
  (High, 3)   -> Four
  (Three,2)   -> Five
  (Pair, 2)   -> Four
  (High, 2)   -> Three
  (Four, 1)   -> Five
  (Three,1)   -> Four
  (TwoPair,1) -> FullHouse
  (Pair, 1)   -> Three
  (High, 1)   -> Pair
  (typ,_)     -> typ

instance Puzzle.Puzzle PuzzleType where
  parseInput = inputParser
  assignment1 = show . assignment1
  assignment2 = show . assignment2

main :: IO ()
main = Puzzle.doMain @PuzzleType

interactive :: FilePath -> IO ()
interactive = Puzzle.interactive @PuzzleType
