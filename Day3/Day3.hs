{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
import qualified Puzzle as Puzzle

import Control.Monad (guard, replicateM)
import Data.List (intersect, splitAt, union)
import Data.Maybe (catMaybes)
import Data.Char
import Text.Parsec hiding (State)
import qualified Text.Parsec.Char  as C
import Text.Parsec.Language (haskellDef)
import qualified Text.Parsec.Token  as P

data Element = Number Integer (Int, Int) | Symbol Char (Int, Int) deriving (Show, Eq)

type PuzzleType = [Element]

inputParser :: Parser PuzzleType
inputParser = catMaybes <$> many (parseDot <|> parseNumber <|> parseSymbol)

sourcePos :: Monad m => ParsecT s u m (Int, Int)
sourcePos = do
  s <- statePos `fmap` getParserState
  return (sourceColumn s, sourceLine s)

parseDot :: Parser (Maybe Element)
parseDot = do
  many1 (symbol ".")
  return Nothing

parseNumber :: Parser (Maybe Element)
parseNumber = do
  p <- sourcePos
  n <- natural
  return $ Just $ Number n p

parseSymbol :: Parser (Maybe Element)
parseSymbol = do
  p <- sourcePos
  s <- oneOf "%!@#$^&*()<>?:;,|\\'[{}]+=-_/"
  return $ Just $ Symbol s p

touching :: (Int, Int) -> [Element] -> [Element]
touching (c, r) ts = do
  t <- ts
  case t of
    (Number n (c', r')) -> do
      guard (r == r' || r == (r' - 1) || r == (r' + 1))
      guard (c - c' <= (length $ show n) && c - c' >= -1)
      return t
    _ -> []

assignment1 :: PuzzleType -> Integer
assignment1 elements = sum $ map (\(Number n _) -> n) $ foldl (process elements) [] elements
  where
    process elements acc (Symbol _ p) = acc `union` touching p elements
    process _ acc (Number _ _) = acc

assignment2 :: PuzzleType -> Integer
assignment2 elements = sum $ foldl (process elements) [] elements
  where
    process elements acc (Symbol '*' p) = case touching p elements of
      [Number g1 _, Number g2 _] -> (g1 * g2) : acc
      _ -> acc
    process elements acc _ = acc

instance Puzzle.Puzzle PuzzleType where
  parseInput = inputParser
  assignment1 = show . assignment1
  assignment2 = show . assignment2

main :: IO ()
main = Puzzle.doMain @PuzzleType

interactive :: FilePath -> IO ()
interactive = Puzzle.interactive @PuzzleType


  -- where
    -- numLen n
    --  | n < 10 = 1
    --  | otherwise = 1 + numLen (n `div` 10)
