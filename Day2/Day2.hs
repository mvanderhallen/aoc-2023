{-# LANGUAGE ScopedTypeVariables #-}
import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Data.Functor (($>))
import Data.Maybe
import Data.List

import Puzzle (lexeme, symbol, ident, natural, int, signed, decimal, whiteSpace, Parser)
import qualified Puzzle as Puzzle

type Game = (Integer, [[(Color, Integer)]])
type PuzzleType = [Game]

inputParser :: Parser PuzzleType
inputParser = many parseGame

data Color = R | G | B deriving (Eq, Show)

parseGame :: Parser (Integer, [[(Color, Integer)]])
parseGame = do 
  id <- symbol "Game " *> natural
  symbol ":"
  rounds <- parseRound `sepBy` symbol ";"
  return (id, rounds)

parseAmount :: Parser (Color, Integer)
parseAmount = do
  n <- natural
  color <- (symbol "red" $> R) <|> (symbol "green" $> G) <|> (symbol "blue" $> B)
  return (color, n)

parseRound :: Parser [(Color,Integer)]
parseRound = parseAmount `sepBy` symbol ","

assignment1 :: PuzzleType -> Integer
assignment1 input = sum $ [fst game |game<-input, let rounds = snd game, not $ any invalid rounds]
  where
    invalid = any invalidColor
    invalidColor (B,n) = n > 14
    invalidColor (R,r) = r > 12
    invalidColor (G,g) = g > 13

assignment2 :: PuzzleType -> Integer
assignment2 input = sum  [power $ requiredFor rounds | game <- input, let rounds = snd game]
  where
    requiredFor rounds = map snd 
      [
        (R, maximum $ map (fromMaybe 0 . lookup R) rounds), 
        (G, maximum $ map (fromMaybe 0 . lookup G) rounds), 
        (B, maximum $ map (fromMaybe 0 . lookup B) rounds)
      ]
    power = product 

instance Puzzle.Puzzle PuzzleType where
  parseInput = inputParser
  assignment1 = show . assignment1
  assignment2 = show . assignment2

main :: IO ()
main = Puzzle.doMain @PuzzleType

interactive :: FilePath -> IO ()
interactive = Puzzle.interactive @PuzzleType
{-
assignment1 :: IO ()
assignment1 = do
  input <- getAOCInput (many parseGame) "input.txt"
  print $ sum $ [fst game |game<-input, let rounds = snd game, not $ any invalid rounds]
  where
    invalid = any invalidColor
    invalidColor (B,n) = n > 14
    invalidColor (R,r) = r > 12
    invalidColor (G,g) = g > 13

assignment2 :: IO ()
assignment2 = do
  input <- getAOCInput (many parseGame) "input.txt"
  print $ sum  [power $ requiredFor rounds | game <- input, let rounds = snd game]
  where
    requiredFor rounds = map snd 
      [
        (R, maximum $ map (fromMaybe 0 . lookup R) rounds), 
        (G, maximum $ map (fromMaybe 0 . lookup G) rounds), 
        (B, maximum $ map (fromMaybe 0 . lookup B) rounds)
      ]
    power = product 

main = do
  assignment1
  assignment2
  -}